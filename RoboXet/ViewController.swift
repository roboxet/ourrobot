//  Accelerometer added
//  ViewController.swift
//  RoboXet
//
//  Created by Murat Koç on 7/27/15.
//  Copyright (c) 2015 Murat Koç. All rights reserved.
//
import UIKit
import CoreMotion

var inp :NSInputStream?
var out :NSOutputStream?
let inputStream = inp!
let outputStream  = out!
var buffer:String = ""
var xcor:String = ""
var ycor:String = ""
var zcor:String = ""


class ViewController: UIViewController
{
    let manager = CMMotionManager()
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var sliderValue: UISlider!
    @IBAction func slider(sender: UISlider)
    {
        let currentVal = Int(sender.value)
        sliderLabel.text = "\(currentVal)"
        buffer = "\(currentVal)"
    }
    @IBAction func buttonPressed(sender: UIButton)
    {
        outputStream.write(buffer, maxLength: count(buffer))
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NSStream.getStreamsToHostWithName("192.168.2.28", port: 10000, inputStream:&inp, outputStream: &out)
        inputStream.open()
        outputStream.open()
        //var readByte :UInt8 = 0
        //while inputStream.hasBytesAvailable
        //{
        //  inputStream.read(&readByte, maxLength: 1)
        //}
        //outputStream.write(buffer, maxLength: count(buffer))
        
        manager.startAccelerometerUpdates()
        let queue = NSOperationQueue.mainQueue
        if manager.accelerometerAvailable
        {
            manager.accelerometerUpdateInterval = 0.01
            manager.startAccelerometerUpdatesToQueue(NSOperationQueue.mainQueue())
            {
                [weak self] (data: CMAccelerometerData!, error: NSError!) in
                
                xcor = "\(data.acceleration.x)"
                ycor = "\(data.acceleration.y)"
                zcor = "\(data.acceleration.z)"
                
                println(" x = " + xcor)
                println(" y = " + ycor)
                println(" z = " + zcor)
                
                if (data.acceleration.y < 0)
                {
                    buffer = "\( 90 - (data.acceleration.y * -90))"
                }
                else if(data.acceleration.y > 0)
                {
                    buffer = "\((data.acceleration.y * 90) + 90)"
                }
                else
                {
                    buffer = "\(90)"
                }
                
            }
        }
        if manager.deviceMotionAvailable
        {
            manager.deviceMotionUpdateInterval = 0.01
            manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue())
            {
                [weak self] (data: CMDeviceMotion!, error: NSError!) in
                let rotation = atan2(data.gravity.x, data.gravity.y) - M_PI
            }
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}

